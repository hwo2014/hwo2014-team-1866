﻿using System;

namespace G12.Messages
{
  	public class BotId
   	{
   		public string name;
   		public string key;
   	}

  	public class Data
   	{
   		public BotId botId;
   		public string trackName;
        public int carCount;

		public Data()
		{
            botId = new BotId();
        }
   	}


	class CreateRace : SendMsg
	{
        public Data data;

		public CreateRace(string name, string key, string trackName, int carCount)
		{
            data = new Data();

			data.botId.name = name;
			data.botId.key = key;
            data.trackName = trackName;
            data.carCount = carCount;
		}

		protected override string MsgType()
		{
			return "createRace";
		}

		protected override Object MsgData()
		{
			return this.data;
		}

	}
}
