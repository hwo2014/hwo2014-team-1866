﻿using System;

namespace G12.Messages
{
	class Turbo : SendMsg
	{
		protected override string MsgType()
		{
			return "turbo";
		}

		protected override Object MsgData()
		{
			return "G12 is on the run";
		}
	}
}
