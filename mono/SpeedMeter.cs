﻿using System;
using G12.Serializations;
using System.IO;

namespace G12.GameItems
{
    public class SpeedMeter
    {
        private Piece[] track;
        private Lane[] lanes;
        public double sumDistance;
        public int cntTicks;
        double lastDistance;
        int lastIndex;
        double distance;

        public SpeedMeter(Piece[] pieces, Lane[] lanes)
        {
            track = pieces;
            this.lanes = lanes;
        }

        public double measureSpeed(PiecePosition position)
        {
            double realRadius = 0;
            //Vzdalenost 2 dilu
            if (lastIndex != position.pieceIndex)
            {
                //oblouk
                //L = (θ × π/180) × r
                if (track[lastIndex].length == 0)
                {
                    if (track[lastIndex].angle < 0)
                        realRadius = Math.Abs(track[lastIndex].radius) + lanes[position.lane.startLaneIndex].distanceFromCenter;
                    else
                        realRadius = Math.Abs(track[lastIndex].radius) - lanes[position.lane.startLaneIndex].distanceFromCenter;
                    distance = (((Math.Abs(track[lastIndex].angle) * Math.PI / 180) * realRadius) - lastDistance) + position.inPieceDistance;
                }
                else
                {
                    distance = (track[lastIndex].length - lastDistance) + position.inPieceDistance;
                }
            }
            //na jednom dilu
            else
            {
                distance = position.inPieceDistance - lastDistance;
            }
            //zmena lajny - zvetseni vzdalenosti o 15%
            if (position.lane.startLaneIndex != position.lane.endLaneIndex)
            {
                //Trinagle hypotenuse
                //c = sqrt(a2 * b2)
                distance = Math.Sqrt(Math.Pow(distance, 2.0) * Math.Pow(Math.Abs(lanes[position.lane.startLaneIndex].distanceFromCenter - lanes[position.lane.endLaneIndex].distanceFromCenter), 2));
            }

            lastIndex = position.pieceIndex;
            lastDistance = position.inPieceDistance;
            sumDistance = sumDistance + distance;
            cntTicks++;
            //speed = distance / tick
            return distance;
        }

        public double getDistanceToCurve(PiecePosition position)
        {
            if (track[position.pieceIndex].angle != 0)
                return 0;
            else
            {
                int i = position.pieceIndex;
                double distance;
                distance = track[i].length - position.inPieceDistance;
                if (i + 1 >= track.Length)
                    i = 0;
                else
                    i++;
                while (track[i].length != 0)
                {
                   distance = distance + track[i].length;
                    i++;
                    if (i >= track.Length)
                        i = 0;
                }
                return distance;
            }
        }

        public double getAvgSpeed()
        {
            return (sumDistance / cntTicks);
        }
    }
}
