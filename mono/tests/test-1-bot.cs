using System;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;
using Newtonsoft.Json;
using G12.GameItems;
using G12.Messages;

namespace G12
{
public class Bot {
    private List<double> values = null;

    public static void Main(string[] args) {
        string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

        Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        using(TcpClient client = new TcpClient(host, port)) {
            NetworkStream stream = client.GetStream();
            StreamReader reader = new StreamReader(stream);
            StreamWriter writer = new StreamWriter(stream);
            writer.AutoFlush = true;

            new Bot(reader, writer, new Join(botName, botKey));
        }
    }

    private StreamWriter writer;
    private int myTick;

    private double SpeedTest(int tick)
    {
        if(tick < values.Count)
            return values[tick];
        else
            return 1.0;
    }

    Bot(StreamReader reader, StreamWriter writer, Join join) {
        this.writer = writer;
        this.myTick = 0;
        this.values = new List<double>();

        System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
        customCulture.NumberFormat.NumberDecimalSeparator = ".";
        System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

        using(TextReader r = File.OpenText("testValues"))
        {
            string l;
            while((l = r.ReadLine()) != null)
            {
                this.values.Add(double.Parse(l, System.Globalization.NumberStyles.Any));
            }
        }

        string line;

        send(join);

        while((line = reader.ReadLine()) != null) {
            MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
            switch(msg.msgType) {
                case "carPositions":
                    send(new Throttle(SpeedTest(myTick)));
                    ++this.myTick;
                    break;
                case "crash":
                    Console.WriteLine("Crashed");
                    System.IO.StreamWriter crashed_file = new System.IO.StreamWriter("crashed");
                    crashed_file.WriteLine(myTick);
                    crashed_file.Close();
                    return;

                case "join":
                    Console.WriteLine("Joined");
                    send(new Ping());
                    break;
                case "gameInit":
                    Console.WriteLine("Race init");
                    send(new Ping());
                    break;
                case "gameEnd":
                    Console.WriteLine("Race ended");
                    System.IO.StreamWriter ticks_file = new System.IO.StreamWriter("ticks");
                    ticks_file.WriteLine(myTick);
                    ticks_file.Close();
                    send(new Ping());
                    break;

                case "gameStart":
                    Console.WriteLine("Race starts");
                    send(new Ping());
                    break;
                default:
                    send(new Ping());
                    break;
            }
        }
    }

    private void send(SendMsg msg) {
        writer.WriteLine(msg.ToJson());
    }
}
}
