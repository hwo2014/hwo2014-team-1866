import os
from subprocess import call

# Settings
beg = 0
end = 1413

# Preparation
values = [float(value.strip()) for value in open("testValues")]
for index in range(end-beg):
    values[index+beg] = 1.0

with open("testValues", "w") as f:
    f.writelines(list("%1.1f\n" % value for value in values))
call(["./run", "testserver.helloworldopen.com", "8091", "ONDRA", "HmNvYmItcYB4ow"])

# Iterative search
while(os.path.isfile("crashed")):
    with open("crashed", "r") as f:
        index = int(f.readline())
        print("Crashing on line "+str(index+1))
        if index < end:
            index -= 1
            while values[index] == 0.0:
                index -= 1
            values[index] = 0.0
            print("Decreasing line "+str(index+1))
        else:
            break

    call(["rm", "crashed"])
    with open("testValues", "w") as f:
        f.writelines(list("%1.1f\n" % value for value in values))
    call(["./run", "testserver.helloworldopen.com", "8091", "ONDRA", "HmNvYmItcYB4ow"])

print("Success")

