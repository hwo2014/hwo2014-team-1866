﻿using System;

namespace G12.Serializations
{
	public class CarPositionInfo
	{
		public CarID id { get; set; }
		public double angle { get; set; }
		public PiecePosition piecePosition { get; set; }
	}

	public class PiecePosition
	{
		public int pieceIndex { get; set; }
		public double inPieceDistance { get; set; }
		public LaneState lane { get; set; }
		public int lap { get; set; }
	}

	public class LaneState
	{
 		public int startLaneIndex {get; set;}
		public int endLaneIndex { get; set; }
	}
}
