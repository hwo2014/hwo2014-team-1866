﻿using System;

using G12.GameItems;
using G12.Serializations;

namespace G12
{
	public abstract class BrainBase
	{
		public double Delta;
		public static double _CPower = 1.601101;

		protected CarInfo _Car;
		protected CarStateManager _CarState;

		public BrainBase(CarInfo car, CarStateManager carState)
		{
			_Car = car;
			_CarState = carState;
		}

		public abstract int SwitchLanes(TrackMap _Map, CTuple<Piece, Piece> currentSegments, TrackState trackState, int pieceIndex);

		public abstract double CalcThrottle(CTuple<Piece, Piece> currentSegments, TrackState trackState);
	}
}
