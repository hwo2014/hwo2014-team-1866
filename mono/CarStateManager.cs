﻿using System;
using System.Collections.Generic;
using System.Linq;
using G12.Serializations;

namespace G12.GameItems
{
	/// <summary>
	///  Object providing track info
	/// </summary>
	public class CarStateManager
	{
		protected string _CarName;
		protected int _LanesCount;
		protected SpeedMeter _SpeedMeter;

		public int RacePosition { get; private set; }
		public int Lap { get; private set; }

		public int LaneIndex { get; private set; }
		public bool IsLaneFree { get; private set; }
		public int[] FreeLanes { get; private set; }
		public double InLaneDistance { get; private set; }

		public double Speed { get; private set; }
		public double PieceIndex { get; private set; }
		public double InPieceDistance { get; set; }

		public double Angle { get; private set; }
		public double AngleDelta { get; private set; }

		public CarStateManager(string name, int lanesCount, SpeedMeter speedMeter)
		{
			_CarName = name;
			_LanesCount = lanesCount;
			_SpeedMeter = speedMeter;
			// TODO nastavit defaulty ?
		}

		public void ChangeState(CarPositionInfo[] cars)
		{
			CarPositionInfo ourCar = cars.FirstOrDefault(c => c.id.name == _CarName);
			var otherCars = cars.Where(c => c.id.name != _CarName);

			SetOurCar(ourCar);
			SetRaceStage(ourCar, otherCars);
		}

		protected void SetOurCar(CarPositionInfo car)
		{
			Lap = car.piecePosition.lap;
			LaneIndex = car.piecePosition.lane.endLaneIndex;
			PieceIndex = car.piecePosition.pieceIndex;
			InPieceDistance = car.piecePosition.inPieceDistance;
			
			// Angle
			AngleDelta = car.angle - Angle;
			Angle = car.angle;

			Speed = _SpeedMeter.measureSpeed(car.piecePosition);			
		}

		protected void SetRaceStage(CarPositionInfo ourCar, IEnumerable<CarPositionInfo> otherCars)
		{
			if(otherCars != null)
			{
                // TODO - neni treba zahrnout i aktualni "lap"?
				var before = otherCars.Where(c => ourCar.piecePosition.pieceIndex < c.piecePosition.pieceIndex ||
					(ourCar.piecePosition.pieceIndex == c.piecePosition.pieceIndex && ourCar.piecePosition.inPieceDistance < c.piecePosition.inPieceDistance));

				if(before != null)
				{
					RacePosition = before.Count();
					CarPositionInfo inBefore = before.FirstOrDefault(c => c.piecePosition.lane.endLaneIndex == ourCar.piecePosition.lane.endLaneIndex);
					IsLaneFree = inBefore == null;

					if(IsLaneFree)
						InLaneDistance = double.MaxValue;
					else // Todo poladit pro prechod mezi kusy!!
						InLaneDistance = inBefore.piecePosition.inPieceDistance + ourCar.piecePosition.inPieceDistance;

					List<int> freeLanes = new List<int>();
					for(int i = 0; i < _LanesCount; i++)
					{
						if(!before.Any(c => c.piecePosition.lane.endLaneIndex == i))
							freeLanes.Add(i);
					}
					FreeLanes = freeLanes.ToArray();					

				}
			}

			// Default
			RacePosition = 0;
			IsLaneFree = true;
			InLaneDistance = double.MaxValue;
			FreeLanes = new int[_LanesCount];
			for(int i = 0; i < _LanesCount; i++)
				FreeLanes[i] = i;
		}
	}
}
