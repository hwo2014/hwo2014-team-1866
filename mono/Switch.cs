﻿using System;

namespace G12.Messages
{
	class Switch : SendMsg
	{
		public string value;

		public Switch(string value)
		{
			this.value = value;
		}

		protected override Object MsgData()
		{
			return this.value;
		}

		protected override string MsgType()
		{
			return "switchLane";
		}
	}
}
