using System;
using System.Linq;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using G12.Serializations;
using G12.GameItems;
using G12.Messages;
using G12.Brains;

namespace G12
{
	public class Bot
	{
		private int _CurrentPieceIndex = -1;
		private string _UsedBotName;
		private CarInfo _CarInfo;
		private TrackMap _Map;
		private CTuple<Piece, Piece> _CurrentPiece;
		private TrackState _CurrentTrackState;
		private CarStateManager _CarState;
		private BrainBase _Brain;
		private bool _InitCompleted = false;

		private bool _TurboUsed = false, _TurboDisabled = false;
		private TurboAvailable _Turbo;
		private bool _TurboAvailable = false;
		private int _TurboTicks = 0;
		private DateTime _TurboEnds;

		public static BrainBase BrainFactory(CarInfo car, CarStateManager stateManager)
		{
			return new RacingBrain1(car, stateManager);
		}

		public static void Main(string[] args)
		{
			string host = args[0];
			int port = int.Parse(args[1]);
			string botName = args[2];
			string botKey = args[3];

			Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

			using(TcpClient client = new TcpClient(host, port))
			{
				NetworkStream stream = client.GetStream();
				StreamReader reader = new StreamReader(stream);
				StreamWriter writer = new StreamWriter(stream);
				writer.AutoFlush = true;

				// Game
				var bot = new Bot(writer, new Join(botName, botKey));

				//var bot = new Bot(writer, new CreateRace(botName, botKey, "germany", 1));
				bot.GameLoop(reader, writer);

			}
		}

		private Bot(StreamWriter writer, Join join)
		{
			BotInit(writer, join);
		}

		private Bot(StreamWriter writer, CreateRace createRace)
		{
			BotInit(writer, createRace);
		}

		private void BotInit(StreamWriter writer, CreateRace createRace)
		{
			_UsedBotName = createRace.data.botId.name;
			Send(writer, createRace);
		}

		private void BotInit(StreamWriter writer, Join join)
		{
			_UsedBotName = join.name;
			Send(writer, join);
		}

		private void GameInit(GameInitData gameData)
		{
			_Map = new TrackMap(gameData);
			_CarInfo = new CarInfo(gameData.race.cars.FirstOrDefault(c => c.id.name == _UsedBotName));
			_CarState = new CarStateManager(_UsedBotName, _Map.LanesCount, new SpeedMeter(gameData.race.track.pieces, gameData.race.track.lanes));
			_Brain = BrainFactory(_CarInfo, _CarState);
		}

		private void CalcCarPosition(StreamWriter writer, CarPositionInfo[] cars)
		{
			_CarState.ChangeState(cars);
			if(_CarState.PieceIndex != _CurrentPieceIndex /*|| _CurrentPiece == null*/)
			{
				_CurrentPieceIndex = (_CurrentPieceIndex + 1) % _Map.PiecesCount;
				_CurrentPiece = _Map.GetPiece(_CurrentPieceIndex);
			}

			_CurrentTrackState = _Map.GetTrackState(_CurrentPieceIndex, _CarState.LaneIndex, _CarState.InPieceDistance, _CurrentTrackState);
			_CarInfo.ReInitPDelta(_CarState.Speed, _CarState.AngleDelta, _CarState.LaneIndex, _CurrentTrackState.CurrentSegment, _Brain);
			// If available -> GO !!!

			if(!_TurboDisabled && _TurboAvailable && DateTime.Now <= _TurboEnds && _Map.IsInLongestSeg(_CurrentTrackState.CurrentSegment, _CurrentTrackState.DistanceToNextRound))
			{
				_CarInfo.Boost = _Turbo.turboFactor;
				_TurboTicks = _Turbo.turboDurationTicks;
				_TurboAvailable = false;
				_TurboUsed = true;
				Send(writer, new Turbo());
				return;
			} 
			else
			{
				if(_TurboTicks-- == 0)
					_CarInfo.Boost = 1;
			}

			int switchLanes = _Brain.SwitchLanes(_Map, _CurrentPiece, _CurrentTrackState, cars[0].piecePosition.pieceIndex);
			if(_InitCompleted)
			{
				if(switchLanes != 0)
				{
					Send(writer, new Switch(switchLanes < 0 ? "Left" : "Right"));
					Console.WriteLine("Switching lane to the " + (switchLanes < 0 ? "left" : "right"));
				} else
					Send(writer, new Throttle(_Brain.CalcThrottle(_CurrentPiece, _CurrentTrackState)));

				if(!double.IsNaN(_CarInfo.CarMass))
					_Map.SetRoundSpeed(BrainBase._CPower, _CarInfo);
			}

		}

		private void GameLoop(StreamReader reader, StreamWriter writer)
		{
			string line;
			while((line = reader.ReadLine()) != null)
			{
				MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
				switch(msg.msgType)
				{
					case "carPositions":
						CalcCarPosition(writer, JsonConvert.DeserializeObject<CarPositionInfo[]>(msg.data.ToString()));
						break;
					case "crash":
						_TurboDisabled = _TurboUsed;
						Console.WriteLine("Crashed");
						break;
					case "turboAvailable":
						Console.WriteLine("Turbo available");
						_TurboAvailable = true;
						_Turbo = JsonConvert.DeserializeObject<TurboAvailable>(msg.data.ToString());
						_TurboEnds = DateTime.Now.AddMilliseconds(_Turbo.turboDurationMilliseconds - 50);
						break;
					case "join":
						Console.WriteLine("Joined");
						Send(writer, new Ping());
						break;
					case "gameInit":
						Console.WriteLine("Race init");
						Send(writer, new Ping());
						GameInit(JsonConvert.DeserializeObject<GameInitData>(msg.data.ToString()));
						break;
					case "gameEnd":
						Console.WriteLine("Race ended");
						Send(writer, new Ping());
						break;
					case "gameStart":
						Console.WriteLine("Race starts");
						Send(writer, new Ping());
						_InitCompleted = true; // Startovaci sekvence obsahuje jednu message carPositions
						break;
					default:
						//Console.WriteLine("Nezachyceno " + msg.msgType.ToString() + " " + msg.data.ToString());
						Send(writer, new Ping());
						break;
				}
			}
		}

		private void Send(StreamWriter writer, SendMsg msg)
		{
			writer.WriteLine(msg.ToJson());
		}
	}
}
