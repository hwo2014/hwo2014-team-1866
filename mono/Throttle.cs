﻿using System;

namespace G12.Messages
{
	class Throttle : SendMsg
	{
		public double value;

		public Throttle(double value)
		{
			this.value = value;
		}

		protected override Object MsgData()
		{
			return this.value;
		}

		protected override string MsgType()
		{
			return "throttle";
		}
	}
}