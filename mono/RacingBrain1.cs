﻿#define NLOG

using System;
using System.Collections.Generic;
using G12.GameItems;
using G12.Serializations;
using System.IO;

namespace G12.Brains
{
	public class RacingBrain1 : BrainBase
	{
		protected int _InitState = 0;
		protected double _ThrottleState;

		protected int _ASegmentID;

		protected int _SwitchState;     // 0 - non |  1 - planed | 2 - switching
		protected int _SwitchDirection; // 0 - non | -1 - left   | 1 - right

		public RacingBrain1(CarInfo car, CarStateManager carState)
			: base(car, carState)
		{
			_SwitchState = 2; // Throttle first -> switch next
			_SwitchDirection = 0;
		}

		public override int SwitchLanes(TrackMap _Map, CTuple<Piece, Piece> currentSegments, TrackState trackState, int pieceIndex)
		{
			switch(_SwitchState)
			{
				case 0:
					_SwitchState = 1;

					int i = pieceIndex;
					// Next switch
					// TODO - must have switch
					while(!_Map.GetPiece(i).Item1.@switch)
						++i;

					double sumLeft = 0;
					double sumRight = 0;
					// Next switch - sum curves
					do
					{
						double length = Math.Abs(2 * Math.PI * _Map.GetPiece(i).Item1.radius * (_Map.GetPiece(i).Item1.@angle / 360));
						if(_Map.GetPiece(i).Item1.@angle < 0)
							sumLeft += length;
						else
							if(_Map.GetPiece(i).Item1.@angle > 0)
								sumRight += length;

						++i;
					} while(!_Map.GetPiece(i).Item1.@switch);

					// TODO _SwitchDirection
					// Which direction
					if(sumLeft == sumRight)
						return 0;
					else
						if(sumLeft > sumRight)
							return -1;
						else
							return 1;

				case 1:
					if(currentSegments.Item1.@switch)
						_SwitchState = 2;
					break;
				case 2:
					if(!currentSegments.Item1.@switch)
						_SwitchState = 0;
					break;
			}
			return 0;
		}

		public override double CalcThrottle(CTuple<Piece, Piece> currentSegments, TrackState trackState)
		{
			// ==== Sumar dostupnych infromaci ====

			//==== Info o aute ====
			//Data ze serveru: _Car.Name .Length .Width .GuideFlagPosition
			//Zrychleni (zmena rychlost pri plynu = 1): _Car.Acceleration
			//Brzdeni (zmena rychlost pri plynu = 0): _Car.Breaking

			//==== Stav auta ====
			//Uhel odklonu a jeho zmena proti poslednimu ticku: _CarState.Angle .AngleDelta
			//Rychlost auta, index casti trati, vzdalenost v dane casti trati: _CarState.Speed .PieceIndex .InPieceDistance
			//Index posledni lane, je tato lane volna, indexy volnych lane (bez aut pred), vzdalenost od soupere pred ve stejne lane: _CarState.LaneIndex .IsLaneFree .FreeLanes .InLaneDistance
			//Pozice v zavode, kolo zavodu: _CarState.RacePosition .Lap

			//==== Aktualni casti trati ==== 
			//Piece, na kterem je aktualne auto: currentSegments.Item1
			//Nasledujici piece, na kterem je aktualne auto: currentSegments.Item1

			//==== Aktualni sjednocene casti trati ====
			//Aktualni cast trati (je na ni auto) - sjednoceni segmentu rovinek a zatacek: TrackState.CurrentSegment
			//Nasledujici zatacka - sjednoceni segmentu rovinek a zatacek: TrackState.NextRound
			//Vzdalenost k nasledujici zatacce: TrackState.DistanceToNextRound

			// Initialization sets acceleration and de-acceleration


			double dDef = default(double);
			if(!_Car.IsInitialized)
				_ThrottleState = DoInit(currentSegments.Item1.radius == dDef);
			else // Simple brain
			{

#if (LOG)
				Console.WriteLine(">STEP");
				Console.WriteLine("Current segment {0}, Next segment {1}", trackState.CurrentSegment.SegmentID, trackState.NextRound.SegmentID);
				Console.WriteLine("Piece index {0}, InPiece distance {1}, LaneInd {2}", _CarState.PieceIndex, _CarState.InPieceDistance, _CarState.LaneIndex);
				Console.WriteLine("{2} - Zatacka je za {0} | {1} ", _CarState.DistanceToCurve, trackState.DistanceToNextRound, _CarState.DistanceToCurve == trackState.DistanceToNextRound);
				Console.WriteLine("Car state");
				Console.WriteLine("Speed {0}, Angle {1}, AngleDelta {2}\n", _CarState.Speed, _CarState.Angle, _CarState.AngleDelta);
#endif
				if(trackState.CurrentSegment.IsRound)
				{
					//Console.WriteLine("S:{0} A:{1} | Angle:{2}", trackState.CurrentSegment.SegmentID, Math.Sign(_CarState.Angle) == Math.Sign(trackState.CurrentSegment.TotalAngle), _CarState.Angle);
					var inputAngle = Math.Sign(_CarState.Angle) == Math.Sign(trackState.CurrentSegment.TotalAngle) ? Math.Abs(_CarState.Angle) : (-1) * Math.Abs(_CarState.Angle);
					_ThrottleState = RoundSegmentThrottle(_CarState.Speed, inputAngle, trackState.CurrentSegment.GetSegmentMinRoundSpeed(_CarState.LaneIndex), trackState.NextRound.GetSegmentMinRoundSpeed(_CarState.LaneIndex), trackState.DistanceToNextRound);
				} else
				{
					_ThrottleState = StraightSegmentThrottle(_CarState.Speed, trackState.NextRound.GetSegmentMinRoundSpeed(_CarState.LaneIndex), /*_CarState.DistanceToCurve);//*/ trackState.DistanceToNextRound);
				}

			}

			_ThrottleState = _ThrottleState > 1 ? 1 : _ThrottleState;
			_ThrottleState = _ThrottleState < 0 ? 0 : _ThrottleState;

			//Console.WriteLine("Current segment {0}, throttle {1}, speed {3}, angle {2} / {4}", trackState.CurrentSegment.SegmentID, _ThrottleState, _CarState.Angle, _CarState.Speed, trackState.CurrentSegment.TotalAngle);
			return _ThrottleState;
		}

		public double DoInit(bool isStraight)
		{
			if(_CarState.Speed != 0)
				_Car.Init(_CarState.Speed);
			return 1.0;
		}

		protected double RoundSegmentThrottle(double actualSpeed, double angle, double safeSpeed, double safeNextRoundSpeed, double distance)
		{
			double wSpeed, wDistance, wAngle;

			// Inicializace
			wSpeed = _Car.GetAcceleration(actualSpeed, _Car.GetSpeedFromThrottle(1)) + actualSpeed;
			wDistance = wSpeed;
			wAngle = ((wSpeed - safeSpeed) * Delta) + angle;

			if(wAngle < 50 && actualSpeed < safeSpeed && (double.IsPositiveInfinity(safeNextRoundSpeed) || wDistance <= distance))
				return 1;


			// Nasleduje rovinka
			while(wDistance <= distance)
			{
				wSpeed = _Car.GetAcceleration(wSpeed, _Car.GetSpeedFromThrottle(1)) + wSpeed;
				wDistance += wSpeed;
				wAngle += (wSpeed - safeSpeed) * Delta;

				if(Math.Abs(wAngle) > 50)
				{
					var pSpeed = StraightSegmentThrottle(_Car.GetAcceleration(actualSpeed, _Car.GetSpeedFromThrottle(safeSpeed)) + actualSpeed, safeNextRoundSpeed, distance);
					return _Car.GetThrottleFromSpeed(safeSpeed < pSpeed ? safeSpeed : pSpeed);
				}
			}

			if(double.IsPositiveInfinity(safeNextRoundSpeed))
				return 1;
			else
				return StraightSegmentThrottle(_Car.GetAcceleration(actualSpeed, _Car.GetSpeedFromThrottle(1)) + actualSpeed, safeNextRoundSpeed, distance);

		}

		protected double StraightSegmentThrottle(double actualSpeed, double targetSpeed, double distance)
		{
			double wSpeed, wDistance;

			// Inicializace
			wSpeed = _Car.GetAcceleration(actualSpeed, _Car.GetSpeedFromThrottle(1)) + actualSpeed;
			wDistance = wSpeed;



			if(wSpeed < targetSpeed)
				return 1;

			while(wSpeed > targetSpeed && wDistance < distance)
			{
				wSpeed = _Car.GetAcceleration(wSpeed, _Car.GetSpeedFromThrottle(0)) + wSpeed;
				wDistance += wSpeed;

				// Ubrzdime - sazime tam bomby dal 
				if(wSpeed <= targetSpeed && wDistance <= distance)
				{
					return 1;
				}
			}

			//Nedobrzdime - brzdici sekvence
			if(actualSpeed - Math.Abs(_Car.GetAcceleration(actualSpeed, _Car.GetSpeedFromThrottle(0))) >= targetSpeed)
				return 0;
			//Nedobrzdime - vracime plyn pro rozdil mezi rychlosti v poslednim stavu a cilovou rychlosti
			else // NIKDY TO TU NEJDE!!!
				return _Car.GetThrottleFromAcceleration(actualSpeed, (targetSpeed - actualSpeed));
		}
	}
}
