﻿using System;
using System.Collections.Generic;
using System.Linq;
using G12;
using G12.Serializations;

namespace G12.GameItems
{
	/// <summary>
	///  Object providing car info
	/// </summary>
	public class CarInfo
	{
		public const double g = 10;

		public bool IsInitialized { get; protected set; }
		public string Name { get; protected set; }
		public double Length { get; protected set; }
		public double Width { get; protected set; }
		public double GuideFlagPosition { get; protected set; }

		public double CarMass { get; private set; }

		protected double _TopSpeed;
		public double TopSpeed 
		{
			get
			{
				return _TopSpeed * Boost;
			}
			private set
			{
				_TopSpeed = value;
			} 
		}

		public double Boost { get; set; }

		public CarInfo(Car car)
		{
			Name = car.id.name;
			Length = car.dimensions.length;
			Width = car.dimensions.width;
			GuideFlagPosition = car.dimensions.guideFlagPosition;
			IsInitialized = false;

			// Init
			Boost = 1;
			CarMass = double.NaN;
		}

		public void Init(double acMax)
		{
			//Acceleration = speed1;
			//Breaking = speed2 - speed1;
			CarMass = 1 / acMax;
			//10 can be tricky - test this with different car specs
			TopSpeed = acMax * g * CarMass;
			BrainBase._CPower = (Math.Pow(5.935, 2) * CarMass) / 100; 


			IsInitialized = true;
		}

		int aSegNum = -1;
		double defDelta = 2.25;
		public void ReInitPDelta(double speed, double angleDelta, int laneIndex, TrackSegment segment, BrainBase brain)
		{
			if(aSegNum != segment.SegmentID)
			{
				brain.Delta = defDelta;
				aSegNum = segment.SegmentID;
			}

			brain.Delta = Math.Abs(angleDelta / (speed - segment.GetSegmentMinRoundSpeed(laneIndex)));
		}

		/// <summary>
		/// Return accleration based on Current and demanded speed.
		/// </summary>
		/// <param name="currentSpeed">Actual car speed</param>
		/// <param name="finalSpeed">Demanded speed - ralted to throttle</param>
		/// <returns></returns>
		public double GetAcceleration(double currentSpeed, double finalSpeed)
		{
			return (finalSpeed - currentSpeed) / (g * CarMass);
		}

		/// <summary>
		/// Return Former speed based on Current and Former Throttle.
		/// </summary>
		public double GetFormerSpeedFromCurrentAndThrottle(double currentSpeed, double formerThrottle)
		{
// Only for breaking with 0 throttle
//			return (currentSpeed - (formerThrottle * TopSpeed) / (g * CarMass)) / (1 - 1 / (g * CarMass));
			return currentSpeed / (1 - 1 / (g * CarMass));
		}

		/// <summary>
		/// Returns throttle equivalent to speed.
		/// </summary>
		/// <param name="speed">Demande speed</param>
		/// <returns></returns>
		public double GetThrottleFromSpeed(double speed)
		{
			return speed / TopSpeed;
		}

		/// <summary>
		/// Returns speed equivalent to throttle.
		/// </summary>
		/// <param name="throttle"></param>
		/// <returns>Demanded throttle</returns>
		public double GetSpeedFromThrottle(double throttle)
		{
			return throttle * TopSpeed;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="finalSpeed"></param>
		/// <param name="acceleration"></param>
		/// <returns></returns>
		public double GetThrottleFromAcceleration(double currentSpeed, double acceleration)
		{
			return (acceleration + currentSpeed) / TopSpeed;
		}
	}

	public class AutoPilot
	{
		public int maneuverDistance;
		public List<double> trackPoints = new List<double>();
	}
}
