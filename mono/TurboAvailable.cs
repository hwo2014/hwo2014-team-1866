﻿using System;

namespace G12.Serializations
{
	public class TurboAvailable
	{
		public double turboDurationMilliseconds { get; set; }
		public int turboDurationTicks { get; set; }
		public double turboFactor { get; set; }
	}
}
