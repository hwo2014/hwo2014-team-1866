﻿using System;

namespace G12.Messages
{
	class Ping : SendMsg
	{
		protected override string MsgType()
		{
			return "ping";
		}
	}
}