﻿#define NLOG

using System;
using System.Linq;
using System.Collections.Generic;
using G12.Serializations;

namespace G12.GameItems
{
	/// <summary>
	///  Object providing track info
	/// </summary>
	public class TrackMap
	{
		// TODO: doresit vsechno co bude potreba z mapy
		bool _MinInitialized = false;
		protected Lane[] _Lanes;
		protected Piece[] _Segments;
		protected List<TrackSegment> _MapSegments;

		public TrackMap(GameInitData data)
		{
			_MapSegments = new List<TrackSegment>();

			LanesCount = data.race.track.lanes.Length;
			PiecesCount = data.race.track.pieces.Length;
			LoadTrackMap(data.race.track.pieces, data.race.track.lanes);
		}

		public int LanesCount { get; private set; }
		public int PiecesCount { get; private set; }

		/// <summary>
		/// Boost ready segment
		/// </summary>
		/// <param name="curSeg"></param>
		/// <param name="distanceToEnd"></param>
		/// <returns></returns>
		public bool IsInLongestSeg(TrackSegment curSeg, double distanceToEnd)
		{
			var straight = _MapSegments.Where(ms => !ms.IsRound);
			if(straight != null)
			{
				if(distanceToEnd / curSeg.GetSegmentLength(0) < 0.66)
					return false;

				var longest = straight.OrderByDescending(ms => ms.GetSegmentLength(0)).Take(2);
				var available = straight.Where(seg => longest.Any(l =>  l.SegmentID == seg.SegmentID || l.GetSegmentLength(0) <= seg.GetSegmentLength(0)));
				
				// Fire boost?
				return available.Any(av => curSeg.SegmentID == av.SegmentID);	 

			}
			return true;
		}

		public CTuple<Piece, Piece> GetPiece(int pieceIndex)
		{
			return new CTuple<Piece, Piece>(_Segments[pieceIndex % _Segments.Length], _Segments[(pieceIndex + 1) % _Segments.Length]);
		}

		public TrackState GetTrackState(int pieceIndex, int laneIndex, double inpieceDistance, TrackState oldState)
		{
			TrackState output = null;

			var lpieceIndex = pieceIndex % _Segments.Length;

			if(oldState != null)
			{
				if(oldState.CurrentSegment.PieceIndexes.Any(pi => pi == lpieceIndex))
					output = oldState;
			}

			if(output == null)
			{
				output = new TrackState();

				// Find current segment and first-next round
				for(int i = 0; i < _MapSegments.Count; i++)
				{
					if(_MapSegments[i].PieceIndexes.Any(pi => pi == lpieceIndex))
					{
						output.CurrentSegment = _MapSegments[i];
						output.NextRound = _MapSegments[(i + 1) % _MapSegments.Count];
						break;
					}
				}
			}

			//Console.Write("!>ind{2}>{0},{1}", output.CurrentSegment.PieceIndexes.IndexOf(lpieceIndex), output.CurrentSegment.PieceIndexes.Count, lpieceIndex);
			// Sum length / angles
			output.DistanceToNextRound = 0; // inexy po spojeni nejsou serazeny dle velikosti, nutnost prepoctu
			for(int i = output.CurrentSegment.PieceIndexes.IndexOf(lpieceIndex); i < output.CurrentSegment.PieceIndexes.Count; i++)
			{
				// Spojeny prvni a posledni index
				if(i > 0 && output.CurrentSegment.PieceIndexes[i] - output.CurrentSegment.PieceIndexes[i - 1] > 1)
					break;

				//Console.Write("{0},", output.CurrentSegment.PieceIndexes[i]);

				if(output.CurrentSegment.IsRound)
					output.DistanceToNextRound += _Segments[output.CurrentSegment.PieceIndexes[i]].angle;
				else
					output.DistanceToNextRound += _Segments[output.CurrentSegment.PieceIndexes[i]].length;
			}
			//Console.Write("\n");

			// Convert sum of angle to round length
			if(output.CurrentSegment.IsRound)
				// Pro jistotu - aby nedoslo k zaokrouhleni - explicitni typo
				output.DistanceToNextRound = (Math.PI * (double)(output.CurrentSegment.Radius - _Lanes[laneIndex].distanceFromCenter) * (double)Math.Abs(output.DistanceToNextRound)) / 180.0;

			// Distract current piece distance
			output.DistanceToNextRound -= inpieceDistance;

			return output;
		}

		public void SetRoundSpeed(double force, CarInfo car)
		{
			if(!_MinInitialized)
			{
				foreach(var seg in _MapSegments.Where(s => s.IsRound))
				{
					foreach(var lane in _Lanes)
					{
						seg.MinRoundSpeed.Add(Math.Sqrt(force * (seg.Radius - lane.distanceFromCenter) / car.CarMass));
					}
				}

				// Min speed for straight segments
				for(int i = 0; i < _MapSegments.Count; ++i)
				{
					if(_MapSegments[i].IsRound)
						continue;

					for(int ii = 0; ii < _Lanes.Length; ++ii)
					{
						double maxInSpeed = _MapSegments[(i + 1) % _MapSegments.Count].MinRoundSpeed[ii];
						double lengthDone = 0;
						double wSpeed = 0;
						while(true)
						{
							wSpeed = car.GetFormerSpeedFromCurrentAndThrottle(maxInSpeed, 0);
							if(lengthDone + wSpeed > _MapSegments[i].Lengths[0])
								break;

							lengthDone += wSpeed;
							maxInSpeed = wSpeed;
						}
						_MapSegments[i].MinRoundSpeed.Add(maxInSpeed);
					}

				}

				//// Min speed for straight segments
				for(int i = _MapSegments.Count - 1; i >= 0; --i)
				{
					for(int ii = 0; ii < _Lanes.Length; ++ii)
					{
						double maxInSpeed = _MapSegments[(i + 1) % _MapSegments.Count].MinRoundSpeed[ii];
						double lengthDone = 0;
						double wSpeed = 0;
						while(true)
						{
							wSpeed = car.GetFormerSpeedFromCurrentAndThrottle(maxInSpeed, 0);
							if(lengthDone + wSpeed > _MapSegments[i].Lengths[0])
								break;

							lengthDone += wSpeed;
							maxInSpeed = wSpeed;
						}
						_MapSegments[i].MinRoundSpeed[ii] = Math.Min(_MapSegments[i].MinRoundSpeed[ii], maxInSpeed);
					}
				}

				_MinInitialized = true;
			}
		}

		private void LoadTrackMap(Piece[] segments, Lane[] lanes)
		{
			_Lanes = lanes;
			_Segments = segments;
			Piece bSegment = segments[0];
			TrackSegment mSeg = InitSegment(segments[0], lanes, 0);
			bool isRound, isBRound = mSeg.IsRound;

			for(int i = 1; i < segments.Length; i++)
			{
				isRound = IsPieceRound(segments[i]);
				if(isRound != isBRound || (isRound && (bSegment.radius != segments[i].radius || (bSegment.angle + segments[i].angle == 0))))
				{
					_MapSegments.Add(mSeg);
					mSeg = InitSegment(segments[i], lanes, i);
				} else
				{
					SetSegment(mSeg, segments[i], lanes, i);
				}

				isBRound = isRound;
				bSegment = segments[i];
			}

			// Merge first and last
			if(_MapSegments[0].IsRound == mSeg.IsRound)
			{
				if(mSeg.IsRound)
				{
					if(_MapSegments[0].Radius == mSeg.Radius && (Math.Sign(_MapSegments[0].TotalAngle) + Math.Sign(mSeg.TotalAngle) != 0))
					{
						_MapSegments[0].TotalAngle += mSeg.TotalAngle;
						mSeg.PieceIndexes.AddRange(_MapSegments[0].PieceIndexes);
						_MapSegments[0].PieceIndexes = mSeg.PieceIndexes;
					} else
						_MapSegments.Add(mSeg);
				} else
				{
					_MapSegments[0].Lengths[0] += mSeg.Lengths[0];
					mSeg.PieceIndexes.AddRange(_MapSegments[0].PieceIndexes);
					_MapSegments[0].PieceIndexes = mSeg.PieceIndexes;
				}
			} else
				_MapSegments.Add(mSeg);


			// Calculates round length
			foreach(var seg in _MapSegments.Where(ms => ms.IsRound))
			{
				for(int i = 0; i < lanes.Length; i++)
					seg.Lengths.Add((Math.PI * (double)(seg.Radius - lanes[i].distanceFromCenter) * Math.Abs(seg.TotalAngle)) / 180.0);
			}

#if (LOG)
			// Testovaci vypis - odstranit
			foreach(var seg in _MapSegments)
			{
				Console.WriteLine("{0}", seg.IsRound ? "ROUND" : "STRAIGHT");
				Console.WriteLine("Segment id: {0}", seg.SegmentID);
				Console.WriteLine("Segment's pieces {0}", string.Join(",", seg.PieceIndexes));
				foreach(int ind in seg.PieceIndexes)
				{
					Piece p = _Segments[ind];
					if(seg.IsRound)
						Console.WriteLine("pIndex {0}, pRadius {1}, pAngle {2}, pSwitch {3}", ind, p.radius, p.angle, p.@switch);
					else
						Console.WriteLine("pIndex {0}, pLength {1}, pSwitch {2}", ind, p.length, p.@switch);
				}
				if(seg.IsRound)
				{
					Console.WriteLine("Segment radius {0}", seg.Radius);
					Console.WriteLine("Segment total angle {0}", seg.TotalAngle);
					Console.WriteLine("Segment legths {0}", string.Join(",", seg.Lengths));
				} else
				{
					Console.WriteLine("Segment legth {0}", seg.Lengths[0]);
				}
			}
			Console.WriteLine("Lanes");
			foreach(var lane in _Lanes)
			{
				Console.WriteLine("lIndex {0} lDistance {0}", lane.index, lane.distanceFromCenter);
			}
			Console.WriteLine("=======================");
#endif

		}

		private TrackSegment InitSegment(Piece segment, Lane[] lanes, int index)
		{
			TrackSegment mSeg = new TrackSegment()
			{
				IsRound = IsPieceRound(segment),
				Radius = segment.radius
			};

			SetSegment(mSeg, segment, lanes, index);

			return mSeg;
		}

		private void SetSegment(TrackSegment tSegment, Piece segment, Lane[] lanes, int index)
		{
			tSegment.TotalAngle += segment.angle;
			tSegment.PieceIndexes.Add(index);

			if(!tSegment.IsRound)
				if(tSegment.Lengths.Count == 0)
					tSegment.Lengths.Add(segment.length);
				else
					tSegment.Lengths[0] += segment.length;
		}

		private bool IsPieceRound(Piece segment)
		{
			return segment.radius != default(int) && segment.angle != default(double);
		}
	}

	public class TrackState
	{
		public TrackSegment CurrentSegment { get; set; }
		public double DistanceToNextRound { get; set; }
		public TrackSegment NextRound { get; set; }
	}

	public class TrackSegment
	{
		private static int SegmentCounter = 0;

		public int SegmentID { get; private set; }
		public bool IsRound { get; set; }
		public int Radius { get; set; }
		public double TotalAngle { get; set; }

		public List<double> MinRoundSpeed { get; set; }
		public List<double> Lengths { get; set; }
		public List<int> PieceIndexes { get; set; }

		public TrackSegment()
		{
			Lengths = new List<double>();
			PieceIndexes = new List<int>();
			MinRoundSpeed = new List<double>();
			SegmentID = SegmentCounter++;
		}

		public double GetSegmentMinRoundSpeed(int laneIndex)
		{
			if(MinRoundSpeed.Count <= laneIndex)
				return double.PositiveInfinity;
			else
				return MinRoundSpeed[laneIndex];
		}

		public double GetSegmentLength(int laneIndex)
		{
			if(!IsRound)
				return Lengths[0];
			else
				return Lengths[laneIndex];
		}
	}
}

